import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import styles from "./Translate.module.scss";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { IoMdArrowRoundBack } from "react-icons/io";
import "react-tabs/style/react-tabs.css";
import { converterEnglish, decodeMorse } from "../../components/Converter";

export default function Translate() {
  const [value, setValue] = useState("");
  const [morseValue, setMorseValue] = useState("");
  const [outputMorse, setOutputMorse] = useState("");
  let output: any[] = [];

  function handleChange(e: React.ChangeEvent<HTMLTextAreaElement>) {
    e.preventDefault();
    setValue(e.target.value);
  }

  useEffect(() => {
    setOutputMorse(decodeMorse(morseValue));
  }, [morseValue]);

  function handleChangeMorse(e: React.ChangeEvent<HTMLTextAreaElement>) {
    e.preventDefault();
    setMorseValue(e.target.value);
  }

  for (let i = 0; i < value.length; i++) {
    output[i] = [];
    for (let x = 0; x < value.length; x++) {
      if (converterEnglish[value[i][x]]) {
        output[i].push(converterEnglish[value[i][x]] + " ");
      }
    }
  }

  return (
    <>
      <div className={styles["container"]}>
        <Link to="/">
          <IoMdArrowRoundBack className={styles["container__icon"]} />
        </Link>
        <h1 className={styles["container__title"]}>Translate</h1>
        <div className={styles["container__tabs"]}>
          <Tabs className={styles["react-tabs"]}>
            <TabList className={styles["react-tabs__tab-list"]}>
              <Tab className={styles["react-tabs__tab"]}>Text to Morse</Tab>
              <Tab className={styles["react-tabs__tab"]}>Morse to Text</Tab>
            </TabList>
            <TabPanel>
              <div className={styles["container__translate"]}>
                <form>
                  <h3>Text</h3>
                  <textarea onChange={handleChange} value={value} />
                </form>
                <h3>Output</h3>
                <div className={styles["output"]}>{output}</div>
              </div>
            </TabPanel>
            <TabPanel>
              <div className={styles["container__translate"]}>
                <form>
                  <h3>Morse</h3>
                  <p>
                    A single space is used to separate the character codes and 2
                    spaces are used to separate words
                  </p>
                  <textarea onChange={handleChangeMorse} value={morseValue} />
                </form>
                <h3>Output</h3>
                <div className={styles["morse-output"]}>{outputMorse}</div>
              </div>
            </TabPanel>
          </Tabs>
        </div>
      </div>
    </>
  );
}
