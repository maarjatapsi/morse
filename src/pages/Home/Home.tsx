import React from "react";
import { Link } from "react-router-dom";
import styles from "./Home.module.scss";
import Button from "../../components/Buttons/Button";

export default function Home() {
  const links = [
    {
      name: "Chart",
      link: "/chart",
    },
    { name: "Translate",
      link: "/translate",
    },
    { name: "Excercises",
      link: "/excercises",
    },
  ];
  return (
    <div className={styles["container"]}>
      <h1 className={styles["container__title"]}>Morse Code</h1>
      <div className={styles["container__buttons"]}>
        {links.map(({ name, link }, index) => (
          <Link key={index} to={link}>
            <Button width="16rem" fontSize="2.4rem" children={name} />
          </Link>
        ))}
      </div>
    </div>
  );
}
