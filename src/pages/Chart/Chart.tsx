import React from "react";
import { Link } from "react-router-dom";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { IoMdArrowRoundBack } from "react-icons/io";
import "react-tabs/style/react-tabs.css";
import styles from "./Chart.module.scss";
import { morse } from "../../data";

export default function Chart() {
  return (
    <>
      <div className={styles["container"]}>
        <Link to="/">
          <IoMdArrowRoundBack className={styles["container__icon"]} />
        </Link>
        <h1 className={styles["container__title"]}>Morse Code Chart</h1>
        <div className={styles["container__tabs"]}>
          <Tabs className={styles["react-tabs"]}>
            <TabList className={styles["react-tabs__tab-list"]}>
              <Tab className={styles["react-tabs__tab"]}>All</Tab>
              <Tab className={styles["react-tabs__tab"]}>Letters</Tab>
              <Tab className={styles["react-tabs__tab"]}>Numbers</Tab>
              <Tab className={styles["react-tabs__tab"]}>Other</Tab>
            </TabList>

            <TabPanel>
              <div className={styles["container__chart"]}>
                {morse.map((chart) => (
                  <div key={chart.id} className={styles["item"]}>
                    <h3>{chart.letter}</h3>
                    <h4>{chart.code}</h4>
                  </div>
                ))}
              </div>
            </TabPanel>
            <TabPanel>
              <div className={styles["container__chart"]}>
                {morse
                  .filter((letter) => letter.id < 30)
                  .map((filteredLetter) => (
                    <div key={filteredLetter.id} className={styles["item"]}>
                      <h3>{filteredLetter.letter}</h3>
                      <h4>{filteredLetter.code}</h4>
                    </div>
                  ))}
              </div>
            </TabPanel>
            <TabPanel>
              <div className={styles["container__chart"]}>
                {morse
                  .filter((number) => number.id > 29 && number.id < 40)
                  .map((filteredNumbers) => (
                    <div key={filteredNumbers.id} className={styles["item"]}>
                      <h3>{filteredNumbers.letter}</h3>
                      <h4>{filteredNumbers.code}</h4>
                    </div>
                  ))}
              </div>
            </TabPanel>
            <TabPanel>
              <div className={styles["container__chart"]}>
                {morse
                  .filter((other) => other.id > 39)
                  .map((filteredNumbers) => (
                    <div key={filteredNumbers.id} className={styles["item"]}>
                      <h3>{filteredNumbers.letter}</h3>
                      <h4>{filteredNumbers.code}</h4>
                    </div>
                  ))}
              </div>
            </TabPanel>
          </Tabs>
        </div>
      </div>
    </>
  );
}
