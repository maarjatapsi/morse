import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { IoMdArrowRoundBack } from "react-icons/io";
import "react-tabs/style/react-tabs.css";
import styles from "./Excercises.module.scss";
import Button from "../../components/Buttons/Button";
import randomWords from "random-words";
import Modal from "react-modal";
import { IoMdClose } from "react-icons/io";
import  { converterEnglish } from "../../components/Converter";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

export default function Excercises() {
  Modal.setAppElement("#root");
  const [modalIsOpen, setIsOpen] = useState(false);
  const [word, setWord] = useState("");
  const [value, setValue] = useState("");
  const [score, setScore] = useState(0);
  const [finalScore, setFinalScore] = useState(0);

  useEffect(() => {
    setWord(() => {
      return randomWords().toLowerCase();
    });
  }, []);

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
    setWord(() => {
      return randomWords().toLowerCase();
    });
    setValue("");
  }

  let morseWord = "";
  let wordWithSpace = "";

  for (let i = 0; i < word.length; i++) {
    morseWord += converterEnglish[word.charAt(i)];
  }

  for (let i = 0; i < word.length; i++) {
    wordWithSpace += converterEnglish[word.charAt(i)] + " ";
  }

  function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.preventDefault();
    setValue(e.target.value);
  }

  const correct: boolean = morseWord === value;

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    openModal();
    if (correct) {
      setScore(score + 1);
    } else {
      setFinalScore(score);
      setScore(0);
    }
  }
  

  return (
    <>
      <div className={styles["container"]}>
        <Link to="/">
          <IoMdArrowRoundBack className={styles["container__icon"]} />
        </Link>
        <h1 className={styles["container__title"]}>Test yourself</h1>
        <div className={styles["excercise"]}>
          <h2>English word</h2>
          <h3>{word}</h3>
          {/*<h3>{morseWord}</h3>*/}
          <h2>Enter the word in morse code</h2>
          <form onSubmit={handleSubmit}>
            <input
              spellCheck="false"
              className={styles["excercise__answer"]}
              onChange={handleChange}
              placeholder="Use . or -"
              value={value}
              required
            />
            <Button width="100%" fontSize="2rem" children="Submit" />
          </form>
        </div>
        <div>
          <Modal
            isOpen={modalIsOpen}
            onRequestClose={closeModal}
            style={customStyles}
          >
            <IoMdClose onClick={closeModal} className={styles["close"]}>
              close
            </IoMdClose>
            <div className={styles["content"]}>
              {correct ? (
                <h4>You were correct! Your score is {score}</h4>
              ) : (
                <>
                  <h4>
                    You were incorrect. Correct answer was: <br />
                    <span>{wordWithSpace}</span>
                  </h4><br/>
                  <h5>Final score: {finalScore}</h5>
                </>
              )}
            </div>
          </Modal>
        </div>
      </div>
    </>
  );
}
