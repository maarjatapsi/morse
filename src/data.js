export const morse = [
  {
    id: 1,
    letter: "A",
    code: ".-",
  },
  {
    id: 2,
    letter: "B",
    code: "-...",
  },
  {
    id: 3,
    letter: "C",
    code: "-.-.",
  },
  {
    id: 4,
    letter: "D",
    code: "-..",
  },
  {
    id: 5,
    letter: "E",
    code: ".",
  },
  {
    id: 6,
    letter: "F",
    code: "..-.",
  },
  {
    id: 7,
    letter: "G",
    code: "--.",
  },
  {
    id: 8,
    letter: "H",
    code: "....",
  },
  {
    id: 9,
    letter: "I",
    code: "..",
  },
  {
    id: 10,
    letter: "J",
    code: ".---",
  },
  {
    id: 11,
    letter: "K",
    code: "-.-",
  },
  {
    id: 12,
    letter: "L",
    code: ".-..",
  },
  {
    id: 13,
    letter: "M",
    code: "--",
  },
  {
    id: 14,
    letter: "N",
    code: "-.",
  },
  {
    id: 15,
    letter: "O",
    code: "---",
  },
  {
    id: 16,
    letter: "P",
    code: ".--.",
  },
  {
    id: 17,
    letter: "Q",
    code: "--.-",
  },
  {
    id: 18,
    letter: "R",
    code: ".-.",
  },
  {
    id: 19,
    letter: "S",
    code: "...",
  },
  {
    id: 20,
    letter: "T",
    code: "-",
  },
  {
    id: 21,
    letter: "U",
    code: "..-",
  },
  {
    id: 22,
    letter: "V",
    code: "...-",
  },
  {
    id: 23,
    letter: "W",
    code: ".--",
  },
  {
    id: 24,
    letter: "X",
    code: "-..-",
  },
  {
    id: 25,
    letter: "Y",
    code: "-.--",
  },
  {
    id: 26,
    letter: "Z",
    code: "--..",
  },
  {
    id: 27,
    letter: "Ä",
    code: ".-.-",
  },
  {
    id: 28,
    letter: "Ö",
    code: "---.",
  },
  {
    id: 29,
    letter: "Ü",
    code: "..--",
  },
  {
    id: 30,
    letter: "0",
    code: "-----",
  },
  {
    id: 31,
    letter: "1",
    code: ".----",
  },
  {
    id: 32,
    letter: "2",
    code: "..---",
  },
  {
    id: 33,
    letter: "3",
    code: "...--",
  },
  {
    id: 34,
    letter: "4",
    code: "....-",
  },
  {
    id: 35,
    letter: "5",
    code: ".....",
  },
  {
    id: 36,
    letter: "6",
    code: "-....",
  },
  {
    id: 37,
    letter: "7",
    code: "--...",
  },
  {
    id: 38,
    letter: "8",
    code: "---..",
  },
  {
    id: 39,
    letter: "9",
    code: "----.",
  },
  {
    id: 40,
    letter: ".",
    code: ".-.-.-",
  },
  {
    id: 41,
    letter: ",",
    code: "--..--",
  },
  {
    id: 42,
    letter: "?",
    code: "..--..",
  },
  {
    id: 43,
    letter: "'",
    code: ".----.",
  },
  {
    id: 44,
    letter: "!",
    code: "-.-.--",
  },
  {
    id: 45,
    letter: "/",
    code: "-..-.",
  },
  {
    id: 46,
    letter: "(",
    code: "-.--.",
  },
  {
    id: 47,
    letter: ")",
    code: "-.--.-",
  },
  {
    id: 48,
    letter: "&",
    code: ".-...",
  },
  {
    id: 49,
    letter: ":",
    code: "---...",
  },
  {
    id: 50,
    letter: ";",
    code: "-.-.-.",
  },
  {
    id: 51,
    letter: "=",
    code: "-...-",
  },
  {
    id: 52,
    letter: "+",
    code: ".-.-.",
  },
  {
    id: 53,
    letter: "-",
    code: "-....-",
  },
  {
    id: 54,
    letter: "_",
    code: "..--.-",
  },
  {
    id: 55,
    letter: '"',
    code: ".-..-.",
  },
  {
    id: 56,
    letter: "$",
    code: "...-..-",
  },
  {
    id: 57,
    letter: "@",
    code: ".--.-.",
  },
  {
    id: 48,
    letter: " ",
    code: " ",
  },
];