import React from "react";
import styles from "./Button.module.scss";

interface Props {
  children?: React.ReactNode;
  width: string;
  fontSize: string;
}

const Button: React.FC<Props> = ({ children, width, fontSize}) => {
  return (
    <button
      className={styles["button"]}
      style={{
        width,
        fontSize,
      }}
    >
      {children}
    </button>
  );
};

export default Button;
