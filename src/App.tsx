import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/Home/Home";
import Chart from "./pages/Chart/Chart";
import Excercises from "./pages/Excercises/Excercises";
import Translate from "./pages/Translate/Translate";


function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/chart" component={Chart} />
        <Route exact path="/translate" component={Translate} />
        <Route exact path="/excercises" component={Excercises} />
      </Switch>
    </Router>
  );
}

export default App;
